/*
 * @Author: CZ
 * @Date: 2022-03-07 19:27:59
 * @LastEditors: CZ
 * @LastEditTime: 2022-03-30 11:46:55
 * @Description: 
 */
const webpack = require("webpack");

module.exports = {
  // devServer: { // 环境配置
  //   host: '0.0.0.0',
  //   public: '172.23.44.42:8080',
  //   port: '8080',
  //   https: false,
  //   disableHostCheck: true,
  //   open: false // 配置自动启动浏览器
  // },
  publicPath: "./",
  configureWebpack: {

    plugins: [

      new webpack.ProvidePlugin({

        $: 'jquery',

        jQuery: 'jquery',

        'window.jQuery': 'jquery',

        Popper: ['popper.js', 'default']

      })

    ]

  }

}